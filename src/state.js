import * as request from "./request.js";


// Single value state functions
// TODO: add the directory ones
// and atomics (are they implemented on the server?)
// TODO also: actual error handling

export function get(key) {
    try {
        return request.get(request.buildUrl("/room/" + window.current_id + "/state/value/" + key))
                      .catch(e => (null));
    } catch (e) {
        return null
    }
}

export function set(key,value) {
    try {
        return request.post(
            request.buildUrl("/room/" + window.current_id + "/state/value/" + key),
            value).catch(e => (null));
    } catch (e) {
        return null
    }
}

export function del(key) {
    try {
        return request.del(request.buildUrl("/room/" + window.current_id + "/state/value/" + key))
                      .catch(e => (null));
    } catch (e) {
        return null
    }
}

