import * as request from "./request.js";

import { log,rerender,update_current_room_state } from "./client.js";
import { cache } from "./cache.js";


// TODO: this should process /-codes
export function submitMessage(input) {
    if (input.charAt(0) == '/') {
        const command = input.slice(1).split(" ")[0];
        // what all do I need
        // changing user + room state
        // moving around
        // really I should just pull from pythia
        // /help           Show this message
        // /nick NAME      Change your name
        // /connect HOST   Connect to a different host server
        // /go DIRECTION   Move around fedispace
        // /go ID          Join a room by ID
        // /create         Create a room where you are
        // /look           View the room description at the current location
        // /describe TEXT  Set the room's description
        // /topic TOPIC    Change the room's topic
        // /roomname NAME  Change the room's name
        // /watch ID NAME  Add a room into your watchlist
        // /unwatch NAME   Remove a room from your watchlist

        switch (command) {
            case "help":
                log(`<pre>
/help           Show this message
/nick NAME      Change your name
/connect HOST   Connect to a different host server
/go DIRECTION   Move around fedispace
/go ID          Join a room by ID
/create         Create a room where you are
-- UNIMPLEMENTED -- 
/look           View the room description at the current location
/describe TEXT  Set the room's description
/topic TOPIC    Change the room's topic
/roomname NAME  Change the room's name
/watch ID NAME  Add a room into your watchlist
/unwatch NAME   Remove a room from your watchlist
</pre>

`);
                break;
            case "nick":
                window.username = input.split(" ")[1];
                localStorage.setItem("username",window.username);
                break;
            case "connect":
                window.homeserver = input.split(" ")[1];
                localStorage.setItem("homeserver",window.homeserver);
                rerender();
                break;
            case "create":
                var url = request.buildUrl("/room/" + window.current_id);
                request.post(url,"");
                log("Room created.");
                break;
            case "go":
                var spl = window.current_id.split(",");
                switch (input.split(" ")[1]) {
                    // Special cases for relative movement
                    case "north":
                    case "up":
                        window.current_id = spl[0] + "," + (parseInt(spl[1])+1);
                        break;
                    case "south":
                    case "down":
                        window.current_id = spl[0] + "," + (parseInt(spl[1])-1);
                        break;
                    case "east":
                    case "right":
                        window.current_id = (parseInt(spl[0])+1) + "," + spl[1];
                        break;
                    case "west":
                    case "left":
                        window.current_id = (parseInt(spl[0])-1) + "," + spl[1];
                        break;

                    // Default case to handle custom room names
                    // and moving to fixed locations
                    default:
                        // Pretend it's always a fixed location for now
                        // TODO: validate the string, check if it's a room id
                        // if not look it up in the directory or room list
                        window.current_id = input.split(" ")[1];
                }
                rerender();
                localStorage.setItem('currentId',window.current_id);
                update_current_room_state(false);
                break;
            default:
                log("Invalid command: " + command);
                break
        }
    } else {
        const url = window.homeserver + "/_fedichat/v1/json/room/" + window.current_id + "/messages";
        const payload = JSON.stringify({from: window.username, type: "text", body: input});
        request.post(url,payload);
    }
}
