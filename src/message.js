// Returns an html string that represents a message
export function render_message(m) {
    switch (m.type) {
        case "image":
            var url = "";
            if (m.url.startsWith("https://") || m.url.startsWith("http://")) {
                url = m.url;                
            } else if (m.url.startsWith("mxc://")) {
                let s = m.url.slice(6).split('/');
                url = "https://" + s[0] + "/_matrix/media/r0/download/" + s[0] + "/" + s[1];
            }
            
            // if it was one of the cases we handle return an image element
            if (url != "") {
                let render = "<span class=\"block\">";
                render += "<span class=\"message_username\">" + m.from + "</span>";
                render += ": ";
                render += "<br>"
                // maybe todo: this span might want message_body class
                render += "<span>" + "<img class=\"image_body\" src=\"" + url + "\">" + "</span>";
                render += "</span>";
                return render;
            }
            // if it's not a case we handle just fallthrough to default
        default: 
            // TODO: this doesn't handle markdown or links
            var render = "<span class=\"block\">"
            render += "<span class=\"message_username\">" + m.from + "</span>";
            render += ": ";
            render += "<span class=\"message_body\">" + m.body.replace(/\n/g, "<br />") + "</span>";
            render += "</span>";
            return render;
    }
}
